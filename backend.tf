terraform {
  backend "gcs" {
    bucket      = "mba-storage"
    prefix      = "dev/state"
    credentials = "user.json"
  }
}